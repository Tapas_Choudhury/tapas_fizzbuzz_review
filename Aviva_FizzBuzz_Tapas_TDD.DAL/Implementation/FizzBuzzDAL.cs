﻿using Aviva_FizzBuzz_Tapas_TDD.DAL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Aviva_FizzBuzz_Tapas_TDD.DAL
{
    /// <summary>
    /// Considerring this as third party service/Data Access Layer. Will be mocked in the UT
    /// </summary>
    public class FizzBuzzDAL : IFizzBuzzDAL
    {
        public virtual string FizzTemplate { get; set; }
        public virtual string BuzzTemplate { get; set; }

        public FizzBuzzDAL()
        {
            FizzTemplate = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Wednesday) ? "Wizz" : "Fizz";
            BuzzTemplate = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Wednesday) ? "Wuzz" : "Buzz";
        }
    }   
}
