﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz_Tapas_TDD.DAL.Domain
{
    public class FizzBuzz
    {
        public string Number { get; set; }
        public string DisplayText { get; set; }        
      }
}
