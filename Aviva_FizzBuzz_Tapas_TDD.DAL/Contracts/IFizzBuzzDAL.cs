﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz_Tapas_TDD.DAL.Contracts
{
    public interface IFizzBuzzDAL
    {
            string FizzTemplate { get; set; }
            string BuzzTemplate { get; set; }
    }
}
