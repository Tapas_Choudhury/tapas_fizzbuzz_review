﻿using Aviva_FizzBuzz_Tapas_TDD.BL;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using Aviva_FizzBuzz_Tapas_TDD.Contract;
using Aviva_FizzBuzz_Tapas_TDD.BL.Contacts;
using Aviva_FizzBuzz_Tapas_TDD.Models;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Contracts;


namespace Aviva_FizzBuzz_Tapas_TDD.ViewModel
{
    enum RuleSet
    {
     Fizz,Buzz   
    }
    public class FizzBuzzViewModel : IFizzBuzzViewModel
    {
        [Required]
        [RegularExpression(@"^[1-9]\d{0,2}$", ErrorMessage = "Please enter a valid maximum no.:1-999")]
        public int MaxNumber { get; set; }
        public string DisplayNumber { get; set; }
        public List<FizzBuzzResultDTO> FizzBuzzNumbers { get; set; }
       
        IFizzBuzzBL FizzBuzzBL;

        public FizzBuzzViewModel(IFizzBuzzBL fizzBuzzBL)
        {
            FizzBuzzNumbers = new List<FizzBuzzResultDTO>();
            FizzBuzzBL = fizzBuzzBL;
        }

        public void GetFizzBuzzNumbers()
        {
            FizzBuzzBL.MaxNumber = this.MaxNumber;
            FizzBuzzBL.RuleSet.Add(RuleSet.Fizz.ToString(),3);
            FizzBuzzBL.RuleSet.Add(RuleSet.Buzz.ToString(),5);
            CommandInvoker InvokeObj = new CommandInvoker();
            InvokeObj.Invoke(FizzBuzzBL);
            FizzBuzzNumbers = AutoMapper.Mapper.Map<List<FizzBuzz>, List<FizzBuzzResultDTO>>(FizzBuzzBL.FizzBuzzNumbers, FizzBuzzNumbers);
           // FizzBuzzNumbers = (List<FizzBuzzResultDTO>)AutoMapper.Mapper.Map(FizzBuzzBL.FizzBuzzNumbers, FizzBuzzNumbers, typeof(List<FizzBuzzResultDTO>), typeof(List<FizzBuzz>));
            //category = (Categoies)AutoMapper.Mapper.Map(viewModel, category, typeof(CategoriesViewModel), typeof(Categoies));
        }
    }

    
}