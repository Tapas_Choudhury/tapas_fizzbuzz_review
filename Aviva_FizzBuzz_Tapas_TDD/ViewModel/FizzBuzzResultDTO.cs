﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aviva_FizzBuzz_Tapas_TDD.ViewModel
{
    public class FizzBuzzResultDTO
    {
        public string Number { get; set; }
        public string DisplayText { get; set; }
    }
}