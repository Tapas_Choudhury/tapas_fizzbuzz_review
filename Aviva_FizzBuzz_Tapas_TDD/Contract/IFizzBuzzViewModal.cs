﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aviva_FizzBuzz_Tapas_TDD.ViewModel;

namespace Aviva_FizzBuzz_Tapas_TDD.Contract
{
        public interface IFizzBuzzViewModel
        {
            int MaxNumber { get; set; }
            List<FizzBuzzResultDTO> FizzBuzzNumbers { get; set; }
            void GetFizzBuzzNumbers();
        }
}
