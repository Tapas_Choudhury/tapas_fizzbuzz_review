﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Aviva_FizzBuzz_Tapas_TDD.ViewModel;
using Aviva_FizzBuzz_Tapas_TDD.BL;
using Aviva_FizzBuzz_Tapas_TDD.Contract;

namespace Aviva_FizzBuzz_Tapas_TDD.Controllers
{
    public class FizzBuzzController : Controller
    {
        public IFizzBuzzViewModel FizzBuzzModel { get; set; }
       
        public FizzBuzzController(IFizzBuzzViewModel fizzBuzzModel)
        {
            FizzBuzzModel = fizzBuzzModel;
        }

        public ActionResult Show()
        {
            return View("Show");
        }
        public ActionResult Submit(string maxNumber)
         {
            if (ModelState.IsValid)
            {
                FizzBuzzModel.MaxNumber = Int32.Parse(maxNumber);
                FizzBuzzModel.GetFizzBuzzNumbers();
                return View("Show", FizzBuzzModel);
            }
            else
            {
                return View("Show", FizzBuzzModel);
            }
        }
    }
}