﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aviva_FizzBuzz_Tapas_TDD.BL.Contacts;

namespace Aviva_FizzBuzz_Tapas_TDD.Models
{
    public class CommandInvoker
    {
        public void Invoke(IFizzBuzzBL cmdObj)
        {
            cmdObj.GetFizzBuzzNumbers();
        }
    }
}