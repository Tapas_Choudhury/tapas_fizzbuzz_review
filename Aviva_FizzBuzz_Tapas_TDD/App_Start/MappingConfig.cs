﻿using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;
using Aviva_FizzBuzz_Tapas_TDD.ViewModel;


namespace Aviva_FizzBuzz_Tapas_TDD.App_Start
{
    public static class MappingConfig
    {
        public static void RegisterMap()
        {
            AutoMapper.Mapper.Initialize(Config => 
            {
                Config.CreateMap<FizzBuzz, FizzBuzzResultDTO>().ReverseMap();
                
                    //.ForMember(dest => dest.Number,
                    //opt => opt.MapFrom(src => src.Number))
                    //.ForMember(dest => dest.DisplayText, opt => opt.MapFrom(src => src.DisplayText));

            });
          
                
        }
    }
}