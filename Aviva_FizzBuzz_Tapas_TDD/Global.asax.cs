﻿using Microsoft.Practices.Unity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Aviva_FizzBuzz_Tapas_TDD.App_Start;
using Aviva_FizzBuzz_Tapas_TDD.BL;
using Aviva_FizzBuzz_Tapas_TDD.ViewModel;
using Aviva_FizzBuzz_Tapas_TDD.DAL;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Contracts;
using Aviva_FizzBuzz_Tapas_TDD.BL.Contacts;
using Aviva_FizzBuzz_Tapas_TDD.Contract;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;

namespace Aviva_FizzBuzz_Tapas_TDD
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            IUnityContainer myContainer = new UnityContainer();
            myContainer.RegisterType<IFizzBuzzDAL, FizzBuzzDAL>();
            myContainer.RegisterType<IFizzBuzzBL, FizzBuzzBL>();
            myContainer.RegisterType<IFizzBuzzViewModel, FizzBuzzViewModel>();
           // myContainer.RegisterType<IController, FizzBuzzController>();
            DependencyResolver.SetResolver(new FizzBuzzDependencyResovler(myContainer));
            MappingConfig.RegisterMap();
        }
    }
}
