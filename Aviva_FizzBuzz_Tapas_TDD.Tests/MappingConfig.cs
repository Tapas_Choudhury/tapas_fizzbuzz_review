﻿using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;
using Aviva_FizzBuzz_Tapas_TDD.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz_Tapas_TDD.Tests
{
    public static class MappingConfig
    {
        public static void RegisterMap()
        {
            AutoMapper.Mapper.Initialize(Config =>
            {
                Config.CreateMap<FizzBuzz, FizzBuzzResultDTO>().ReverseMap();

                //.ForMember(dest => dest.Number,
                //opt => opt.MapFrom(src => src.Number))
                //.ForMember(dest => dest.DisplayText, opt => opt.MapFrom(src => src.DisplayText));

            });


        }
    }
}
