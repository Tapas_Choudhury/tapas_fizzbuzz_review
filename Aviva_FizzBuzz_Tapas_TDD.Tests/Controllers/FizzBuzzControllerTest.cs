﻿using Moq;
using NUnit.Framework;
using System.Linq;
using System.Web.Mvc;

using Aviva_FizzBuzz_Tapas_TDD.BL;
using Aviva_FizzBuzz_Tapas_TDD.Controllers;
using Aviva_FizzBuzz_Tapas_TDD.DAL;
using Aviva_FizzBuzz_Tapas_TDD.ViewModel;
using Aviva_FizzBuzz_Tapas_TDD.Contract;
using Aviva_FizzBuzz_Tapas_TDD.App_Start;

namespace Aviva_FizzBuzz_Tapas_TDD.Tests.Controllers
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {

        public FizzBuzzControllerTest()
        {
            MappingConfig.RegisterMap();
        }


        [Test]
        public void Get_Show_View_Name()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Show() as ViewResult;
            

            //Assert
            Assert.AreEqual("Show",result.ViewName);
        }

        [Test]
        public void Give_4_Get_12Fizz4()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Submit("4") as ViewResult;
            var model = result.Model as IFizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Fizz4", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }

        [Test]
        public void Give_5_Get_12Fizz4Buzz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Submit("5") as ViewResult;
            var model = result.Model as IFizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Fizz4Buzz", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }

        [Test]
        public void Give_15_Get_FizzBuzz()
        {
            //Arrange
            var mock = new Mock<FizzBuzzDAL>();
            mock.Setup(q => q.FizzTemplate).Returns("Fizz");
            mock.Setup(q => q.BuzzTemplate).Returns("Buzz");
            var controller = new FizzBuzzController(new FizzBuzzViewModel(new FizzBuzzBL(mock.Object)));

            //Act
            ViewResult result = controller.Submit("15") as ViewResult;
            var model = result.Model as IFizzBuzzViewModel;

            //Assert
            Assert.AreEqual("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", string.Concat(model.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }
    }
}
