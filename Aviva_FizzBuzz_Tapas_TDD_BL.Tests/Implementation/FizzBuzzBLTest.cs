﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using NUnit.Framework;
using Aviva_FizzBuzz_Tapas_TDD.BL;
using Aviva_FizzBuzz_Tapas_TDD.DAL;
using System.Collections.Generic;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;

namespace Aviva_FizzBuzz_Tapas_TDD_BL.Tests
{
    [TestFixture]
    public class FizzBuzzBLTest
    {
       

        [Test]
        public void Match_Count()
        {
            //Arrange
            FizzBuzzBL fizzBuzzBL = new FizzBuzzBL(new FizzBuzzDAL());
            Dictionary<string,int> ruleSet= new Dictionary<string,int>();
            ruleSet.Add("Fizz",3);
            ruleSet.Add("Buzz",5);

            //Act
            fizzBuzzBL.MaxNumber = 4;
            fizzBuzzBL.RuleSet = ruleSet;
            fizzBuzzBL.GetFizzBuzzNumbers();

            //Assert
            NUnit.Framework.Assert.AreEqual(fizzBuzzBL.MaxNumber, fizzBuzzBL.FizzBuzzNumbers.Count);
        }

        [Test]
        public void Give_5_Get_12Fizz4Buzz()
        {
            //Arrange
            FizzBuzzBL fizzBuzzBL = new FizzBuzzBL(new FizzBuzzDAL());
            Dictionary<string, int> ruleSet = new Dictionary<string, int>();
            ruleSet.Add("Fizz", 3);
            ruleSet.Add("Buzz", 5);

            //Act
            fizzBuzzBL.MaxNumber = 5;
            fizzBuzzBL.RuleSet = ruleSet;
            fizzBuzzBL.GetFizzBuzzNumbers();

            //Assert
            NUnit.Framework.Assert.AreEqual("12Fizz4Buzz", string.Concat(fizzBuzzBL.FizzBuzzNumbers.Select(q => q.DisplayText)));
        }
    }
}
