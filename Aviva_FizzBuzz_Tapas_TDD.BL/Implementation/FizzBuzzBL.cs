﻿using Aviva_FizzBuzz_Tapas_TDD.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Contracts;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;
using Aviva_FizzBuzz_Tapas_TDD.BL.Contacts;

namespace Aviva_FizzBuzz_Tapas_TDD.BL
{
    public class FizzBuzzBL : IFizzBuzzBL
    {
        public IFizzBuzzDAL FizzBuzzData { get; set; }
        public int MaxNumber { get; set; }
        public Dictionary<String,int> RuleSet { get; set; }
        public List<FizzBuzz> FizzBuzzNumbers { get; set; }

        public FizzBuzzBL(IFizzBuzzDAL data)
        {
            FizzBuzzData = data;
            RuleSet = new Dictionary<string, int>();
            FizzBuzzNumbers = new List<FizzBuzz>();
        }

        public void GetFizzBuzzNumbers()
        {
            for (int i = 1; i <= MaxNumber; i++)
            {
                FizzBuzz businessObj = new FizzBuzz();
                businessObj.Number = i.ToString();
                if (i % RuleSet[FizzBuzzData.FizzTemplate] == 0 && i % RuleSet[FizzBuzzData.BuzzTemplate] == 0)
                    businessObj.DisplayText = FizzBuzzData.FizzTemplate + FizzBuzzData.BuzzTemplate;
                else if (i % RuleSet[FizzBuzzData.FizzTemplate] == 0)
                    businessObj.DisplayText = FizzBuzzData.FizzTemplate;
                else if (i % RuleSet[FizzBuzzData.BuzzTemplate] == 0)
                    businessObj.DisplayText = FizzBuzzData.BuzzTemplate;
                else
                    businessObj.DisplayText = i.ToString();

                FizzBuzzNumbers.Add(businessObj);
            }

        }
    }

    
}
