﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Contracts;
using Aviva_FizzBuzz_Tapas_TDD.DAL.Domain;

namespace Aviva_FizzBuzz_Tapas_TDD.BL.Contacts
{ 
        public interface IFizzBuzzBL
        {
            IFizzBuzzDAL FizzBuzzData { get; set; }
            int MaxNumber { get; set; }
            Dictionary<String,int> RuleSet { get; set; }
            List<FizzBuzz> FizzBuzzNumbers { get; set; }
            void GetFizzBuzzNumbers();
        }       
}
    